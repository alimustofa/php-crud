<?php
require_once("config.php");
	$action = $_GET['action'];

	if ($action == "add") {
		if(isset($_POST['submit'])) {
			$nama = $_POST['nama'];
			$alamat = $_POST['alamat'];
			$email = $_POST['email'];

			$ekstensi_diperbolehkan	= array('png','jpg');
			$filename = $_FILES['foto']['name'];
			$x = explode('.', $filename);
			$ekstensi = strtolower(end($x));
			$ukuran	= $_FILES['foto']['size'];
			$file_tmp = $_FILES['foto']['tmp_name'];	
 
			if(in_array($ekstensi, $ekstensi_diperbolehkan) === true){
				if($ukuran < 1044070){			
					move_uploaded_file($file_tmp, 'file/'.$filename);
					
					$query = mysqli_query($mysqli, "INSERT INTO user(nama,alamat,email,foto) VALUES ('$nama','$alamat','$email', '$filename')");

					if($query){
						header('location: index.php');
					}else{
						echo 'GAGAL MENGUPLOAD GAMBAR';
					}
				}else{
					echo 'UKURAN FILE TERLALU BESAR';
				}
			}else{
				echo 'EKSTENSI FILE YANG DI UPLOAD TIDAK DI PERBOLEHKAN';
			}

		}
	} elseif ($action == "update") {
		if(isset($_POST['submit'])) {
			$id = $_POST['id'];
			$nama = $_POST['nama'];
			$alamat = $_POST['alamat'];
			$email = $_POST['email'];

			$result = mysqli_query($mysqli, "UPDATE user set nama='$nama', alamat='$alamat', email='$email' where id='$id'");

			header('location: index.php');
		}
	} else if ($action == "delete") {
		$id = $_GET['id'];
		if (!isset($id)) {
			echo "ID Kosong";
		} else {
			$data = mysqli_query($mysqli, "SELECT * FROM user where id='$id'");
			$len = mysqli_num_rows($data);
			
			if ($len == 0) {
				echo "Data tidak ditemukan";
			} else {
				$result = mysqli_query($mysqli, "DELETE FROM user where id='$id'");
		
				header('location: index.php');
			}
		}

	}
?>